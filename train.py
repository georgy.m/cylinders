from data import MetaDataset, RowsDataset, collate, DatasetInfo
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import DataLoader
import torch.optim as optim
from adapters import get_adapter
from cores import MainCore, TinyCore
from model import MultiModel, Model
from math import ceil
import torch
import time
import os


print(time.asctime())
cuda = False
if cuda:
    assert torch.cuda.is_available()
run_folder = 'runs'
writer = SummaryWriter(run_folder)

def load_adaptors(core, d: dict, tensors: list[torch.Tensor],
                 metas: list[DatasetInfo]):
    global run_folder
    adapters_a, adapters_b = list(), list()
    rotators, priors = list(), list()
    for tensor, meta in zip(tensors, metas):
        aid = meta.adapter_id
        adapters = d.get(aid, None)
        if adapters is None:
            items = get_adapter(tensor.shape[1], core, meta)
            adapter_a, adapter_b, rotator, prior = items    
            path = os.path.join(run_folder, aid)
            if os.path.isfile(path):
                checkpoint = torch.load(path)
                adapter_a.load_state_dict(checkpoint['a'])
                adapter_b.load_state_dict(checkpoint['b'])
                for p in prior:
                    p.load_state_dict(checkpoint[p.name])
                rotator.load_state_dict(checkpoint['rotator'])
            d[aid] = (adapter_a, adapter_b, rotator, prior)
        else:
            adapter_a, adapter_b, rotator, prior = adapters
        adapters_a.append(adapter_a)
        adapters_b.append(adapter_b)
        rotators.append(rotator)
        priors.append(prior)
    return adapters_a, adapters_b, rotators, priors

        
def get_optimizer(core, adapters):
    models = [core]
    for (a, b, r, priors) in adapters.values():
        models.append(a)
        models.append(b)
        models.append(r)
        list(map(lambda x: models.append(x), priors))
    return optim.AdamW((p for m in models for p in m.parameters()))

def get_plots(metadata, core, adapters, suptitle=None):
    from matplotlib import pyplot as plt
    dl = DataLoader(metadata, 1, shuffle=False, collate_fn=collate)
    plots = list()
    plots = plt.figure()
    num_datasets = len(metadata)
    c = 1
    lf = 0
    for tensor, meta in dl:
        tensor = tensor[0]
        meta = meta[0]
        a, b, rotator, priors = adapters[meta.adapter_id]
        m = Model(a, b, core, meta=meta)
        if cuda:
            m = m.cuda()
            priors = list(map(lambda x: x.cuda(), priors))
        samples = DataLoader(tensor, 512, shuffle=False)
        emb = None
        d = dict()
        its = list()
        n = len(samples)
        for sample in samples:
            _, x_cl = meta.cut_tensor(sample)
            x_cl = x_cl[:1]
            if cuda:
                sample = sample.cuda()
            loss, z = m.loss(sample, return_embedding=True, priors=priors)
            lf += loss
            if emb is None:
                emb = z
            else:
                emb = torch.cat([emb, z])
            try:
                lbl = next(iter(x_cl))
                for lb in lbl:
                    l = ''.join(map(str, map(int, lb)))
                    i = d.get(l, None)
                    if i is None:
                        i = len(d) + 1
                        d[l] = i
                    its.append(i)
            except StopIteration:
                pass
        # f = plt.figure()
        lf /= n
        plt.subplot(int(ceil(num_datasets / 2)), 2, c)
        # ax = axes[c]
        c += 1
        if not its:
            its = 1
        plt.scatter(emb[:, 0].tolist(), emb[:, 1].tolist(),)
        plt.title(f'({meta.organism}){meta.experiment_name}')
        # plots.append(f)
    if suptitle:
        plt.suptitle(suptitle)
    return plots, lf, emb
            
            
            

n_epochs = 1000
n_sub_epochs = 1
d_adapters = dict()
core = MainCore()
# core = TinyCore()
if cuda:
    core = core.cuda()
metadata = MetaDataset()
dataloader = DataLoader(metadata, 3, shuffle=True, collate_fn=collate)
for tensors, metas in dataloader:
    load_adaptors(core, d_adapters, tensors, metas)


path = os.path.join(run_folder, core.__class__.__name__)
if os.path.isfile(path):
    core.load_state_dict(torch.load(path))
opt = get_optimizer(core, d_adapters)
lf0 = None
lf = None
print(time.asctime(), "Training...")
for epoch in range(n_epochs):
    with torch.no_grad():
            if lf0 is None:
                f, lf0, emb = get_plots(metadata, core, d_adapters,
                                        suptitle=str(0))
                writer.add_figure('Embedding', f, 0)
            else:
                lf0 = lf
    for tensors, metas in dataloader:
        adapters_a, adapters_b, rotators, priors = load_adaptors(core,
                                                                 d_adapters,
                                                                 tensors,
                                                                 metas)
        ds_samples = RowsDataset(tensors)
        dl_samples = DataLoader(ds_samples, 512, shuffle=True)
        
        model = MultiModel(adapters_a, adapters_b, core, metas)
        if cuda:
            model = model.cuda()
            priors = list(map(lambda x: x.cuda(), priors))

        for _ in range(n_sub_epochs):
            for samples in dl_samples:
                if cuda:
                    samples = [t.cuda() for t in samples]
                opt.zero_grad()
                lf = model.loss(samples, priors=priors)
                lf.backward()
                opt.step()
    with torch.no_grad():
        f, lf, emb = get_plots(metadata, core, d_adapters,
                               suptitle=str(epoch))
        writer.add_figure('Embedding', f, epoch)
        writer.add_scalar('training loss', lf, epoch)
    print(time.asctime(), epoch, lf0, lf)
        # TODO: Save model
writer.close()