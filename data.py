#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sklearn.preprocessing import OneHotEncoder
from dataclasses import dataclass
from torch.utils.data import Dataset
from copy import deepcopy
import datatable as dt
import numpy as np
import torch
import os

torch_type = torch.get_default_dtype()
numpy_type = getattr(np, str(torch_type).split('.')[-1])


data_folder = 'datasets'

@dataclass
class DatasetInfo:
    """Structure for keeping metadata for a dataset."""

    __slots__ = ['experiment_name', 'type', 'organism', 'cols', 'eid',
                 'adapter', 'adapter_id', 'file']

    experiment_name: str
    type: str
    organism: str
    cols: list[str]
    eid: int
    adapter: str
    adapter_id: str
    file: str
    
    def get_count_start(self):
        if not self.cols:
            return 0
        return self.cols[-1][-1]

    def cut_tensor(self, tensor: torch.Tensor):
        """
        Divides tensor into its main counts part and classifier/regression
        parts.

        Parameters
        ----------
        tensor : torch.Tensor
            Inpute tensor.

        Returns
        -------
        x : torch.Tensor
            Count matrix.
        y : list[torch.Tensor]
            OneHotEncoded classes.

        """
        cols = self.cols
        if cols:
            b = cols[-1][-1]
            x = tensor[..., :, b:]
            y = [tensor[..., :, a:b] for _, a, b in cols]
            return x, y
        return tensor, list()


class MetaDataset(Dataset):
    """
    Yields datasets that correspond to different experiments.
    
    When used with DataLoader, custom collate_fn should be used as shapes
    don't agree.
    """
    def __init__(self, meta=None):
        if meta is None:
            serialize_datasets(rewrite=False)
            meta = get_datasets_meta()
        self.meta = meta

    def __len__(self):
        return len(self.meta)

    def __getitem__(self, idx):
        meta = self.meta[idx]
        tensors = torch.load(meta.file)
        tensor = tensors['counts']
        if type(tensor) is np.ndarray:
            tensor = torch.from_numpy(tensor)
        tensor = tensor.type(torch_type)
        cls_tensors = list()
        cols = list()
        b = 0
        for c in meta.cols:
            if type(c) is tuple:
                c, a, b = c
                x = tensors[c]
            else:
                a = b
                x = tensors[c]
                b = a + x.shape[1]
            if type(x) is np.ndarray:
                x = torch.from_numpy(x)
            x = x.type(torch_type)
            cls_tensors.append(x)
            cols.append((c, a, b))
        meta.cols = cols
        tensor = torch.cat([tensor] + cls_tensors, dim=1)
        return tensor, meta
    

class RowsDataset(Dataset):
    """Produces different subdatasets from 2D tensor rows."""
    def __init__(self, data, shuffle=True):
        self.min_n = min(t.shape[0] for t in data)
        if shuffle:
            self.inds = [torch.randperm(self.min_n) for _ in data]
        else:
            self.inds = [torch.arange(end=self.min_n)] * len(data)
        self.data = data
        

    def __len__(self):
        return self.min_n

    def __getitem__(self, idx):
        return tuple(t[inds[idx]] for t, inds in zip(self.data, self.inds))

class ColsDataset(Dataset):
    """
    Produces different subdatasets from 2D tensor columns.
    
    When used with DataLoader, custom collate_fn should be used.
    """
    def __init__(self, data: list[torch.Tensor], num_chunks: int,
                 meta=None, shuffle=True):
        if meta:
            k = meta.get_count_start()
        else:
            k = 0
        if shuffle:
            inds = [torch.randperm(t.shape[1] - k) + k for t in data]
        else:
            inds = [torch.arange(start=k, end=t.shape[1]) for t in data]
        lt = list()
        ar = torch.arange(end=k)
        for i in num_chunks:
            tmp = list()
            for ind in inds:
                n = len(ind)
                t = n // num_chunks
                a = n + t * i
                b = a + t
                tmp.append(torch.cat([ar, inds[a:b]]))
            lt.append(tmp)
        self.inds = lt
        self.num_chunks = num_chunks
        self.data = data

    def __len__(self):
        return self.num_chunks

    def __getitem__(self, idx):
        return tuple(t[:, inds] for t, inds in zip(self.data, self.inds[idx]))
    

def collate(batch):
    return list(zip(*batch))
    


def get_datasets_info():
    """
    Retrieve information on datasets stored in .txt data_ files.

    Raises
    ------
    SyntaxError
        Occures when one of the parameters is incorrectly specified.

    Returns
    -------
    results : List[dict]
        List of key->rvalue mappings for each dataset.

    """
    
    results = list()
    base_d = {'organisms': str(), 'type': str(),  'metacols': list(),
              'cols': list(), 'adapter': str(), 'adapter_id': str(),
              'name': str()}
    fs = filter(lambda x: x.startswith('data_') and x.endswith('.txt'),
                os.listdir(data_folder))
    fs = map(lambda x: os.path.join(data_folder, x), fs)
    for f in fs:
        with open(f, 'r') as f:
            lines = map(lambda x: list(map(lambda y: y.strip(), x.split('='))),
                        filter(lambda x: '=' in x, f.readlines()))
        d = deepcopy(base_d)
        for lval, rval in lines:
            if lval == 'organisms':
                d[lval] = tuple(map(lambda x: x.strip(), rval.split(',')))
            elif lval in ('type', 'name'):
                d[lval] = rval
            elif lval in ('cols', 'metacols'):
                d[lval] = tuple(map(lambda x: x.strip(), rval.split(',')))
            elif lval == 'adapter':
                d[lval] = rval
            elif lval == 'adapter_id':
                d[lval] = rval
            else:
                raise SyntaxError(f"Unknown property {lval} in {f}.")
        results.append(d)
    return results

def serialize_datasets(info=None, rewrite=True):
    """
    Transforms tabular data to tensor format and saves it for further faster
    use.

    Parameters
    ----------
    info : List[dict], optional
        Info on datasets as returned by get_datasets_info. If None, then 
        get_datasets_info is called. The default is None.
    rewrite : bool, optional
        If True, then tensor files are overwritten if they exist. The default
        is True.

    Returns
    -------
    None.

    """
    
    if info is None:
        info = get_datasets_info()
    for d in info:
        path = os.path.join(data_folder, d['name'])
        species = d['organisms']
        data_files = filter(lambda x: x.startswith(species) and \
                            x.endswith(('.csv', '.tsv', '.txt', '.npy')) and \
                            os.path.isfile(os.path.join(path, x)),
                                           os.listdir(path))
        metacols = d['metacols']
        for file in data_files:
            cols = {c: None for c in d['cols']}
            file = os.path.join(path, file)
            for i in reversed(range(len(file))):
                if file[i] == '.':
                    break
            pt_file = file[:i] + '.pt'
            if not rewrite and os.path.isfile(pt_file):
                continue
            if file.endswith('.npy'):
                x = np.load(file)
                metacols = list(map(int, metacols))
                cols = list(map(int, cols))
                if cols:
                    ohe = OneHotEncoder(sparse=False)
                    cols = {c: torch.from_numpy(ohe.fit_transform(x[:, i]))
                            for c in cols}
                    metacols += list(cols.keys())
                if metacols:
                    x = np.delete(x, metacols, axis=1)
            else:
                x = dt.fread(file)
                if metacols:
                    del x[:, metacols]

                if cols:
                    ohe = OneHotEncoder(sparse=False)
                    cols = {c: torch.from_numpy(ohe.fit_transform(x[c].to_numpy()))
                            for c in cols}
                    del x[:, list(cols.keys())]
                x = x.to_pandas().T.drop_duplicates()
                x = x.replace('True', 1.0)
                x = x.replace('False', 0.0)
                x = x.values.T

            x = x.astype(numpy_type)
            x -= x.mean(axis=0)
            stds = x.std(axis=0)
            x = x[:, stds > 0.001]
            x = (x.T / x.sum(axis=1)).T
            x = torch.from_numpy(x)
            res = cols
            res['counts'] = x
            torch.save(res, pt_file)

def get_datasets_meta(info=None):
    """
    Get a list of metadata on available datasets.

    The only method from this submodule that is assumed to be ran by an
    external user.
    Parameters
    ----------
    info : List[dict], optional
        Info on datasets as returned by get_datasets_info. If None, then 
        get_datasets_info is called. The default is None.

    Returns
    -------
    res : TYPE
        DESCRIPTION.

    """
    
    if info is None:
        info = get_datasets_info()
    res = list()
    for d in info:
        name = d['name']
        tp = d['type']
        orgs = d['organisms']
        adapter = d['adapter']
        adapter_id = d['adapter_id']
        cols = d['cols']
        path = os.path.join(data_folder, name)
        for org in orgs:
            files = filter(lambda x: x.endswith('.pt') and x.startswith(org) \
                           and os.path.isfile(os.path.join(path, x)),
                           os.listdir(path))
            for i, file in enumerate(sorted(files)):
                file = os.path.join(path, file)
                aid = adapter_id
                if not aid:
                    aid = '_'.join((name, tp, org, str(i + 1)))
                d = DatasetInfo(experiment_name=name, type=tp, organism=org,
                                cols=cols, eid=i + 1, adapter=adapter,
                                adapter_id=aid, file=file)
                res.append(d)
    return res
