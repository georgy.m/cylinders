#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Cylinder skeleton"""
import torch.nn as nn


class MultiModel(nn.Module):
    def __init__(self, adapters_a: list[nn.Module],
                 adapters_b: list[nn.Module], core: nn.Module, metas: list):
        super().__init__()
        self.adapters_a = adapters_a
        self.adapters_b = adapters_b
        self.core = core
        self.models = [nn.Sequential(a, core, b) for a, b in zip(adapters_a,
                                                                 adapters_b)]
        self.models = nn.ModuleList(self.models)
        self.mse_loss = nn.MSELoss()
        self.metas = metas
    
    def embedding(self, x, use_meta=True):
        if not use_meta:
            return [self.core.embedding(adapter(tx))
                    for adapter, tx in zip(self.adapters_a, x)]
        return [self.core.embedding(adapter(m.cut_tensor(tx)[0]))
                    for adapter, tx, m in zip(self.adapters_a, x, self.metas)]

    def forward(self, x, use_meta=True):
        if not use_meta:
            return [model(tx) for tx, model in zip(x, self.models)]
        return [model(meta.cut_tensor(tx)[0])
                for tx, model, meta in zip(x, self.models, self.metas)]
    
    def loss(self, x, alpha_classifier=0.1, alpha_sim=0.5, priors=None,
             grad_adapters=True, grad_priors=True, grad_core=True,
             ret_all=False):
        list(map(lambda x: x.requires_grad_(grad_adapters), self.adapters_a))
        list(map(lambda x: x.requires_grad_(grad_adapters), self.adapters_b))
        if priors is None:
            priors = [list()] * len(self.adapters_a)
        assert len(priors) == len(self.adapters_a)
        for prior in priors:
            list(map(lambda x: x.requires_grad_(grad_priors), prior))
        core = self.core
        core.requires_grad_(grad_core)
        lf_mse = 0
        lf_clf = 0
        lf_sim = 0
        loss = self.mse_loss
        nc = 1
        for x, ad_a, ad_b, priors, meta in zip(x, self.adapters_a,
                                               self.adapters_b,
                                               priors, self.metas):
            x, cl_x = meta.cut_tensor(x)
            x0 = x
            x = core.encode(ad_a(x))
            lf = 0
            c = 0
            for prior, xl in zip(priors, cl_x):
                lf += prior.loss(x, xl)
                c += 1
            if c:
                nc += 1
                lf /= c
                lf_clf += lf
            lf_mse += loss(ad_b(core.decode(x)), x0)
        lf_mse /= len(x)
        lf_clf /= nc
        lf_sim /= len(x)
        lf = lf_mse + alpha_sim * lf_sim + alpha_classifier * lf_clf
        if not ret_all:
            return lf
        return lf, (lf_mse, lf_clf, lf_sim)     



class Model(nn.Module):
    def __init__(self, adapter_a: nn.Module, adapter_b: nn.Module,
                 core: nn.Module, meta):
        super().__init__()
        self.adapter_a = adapter_a
        self.adapter_b = adapter_b
        self.core = core
        self.model = nn.Sequential(adapter_a, core, adapter_b)
        self.meta = meta
        self.mse_loss = nn.MSELoss()
    
    def embedding(self, x, use_meta=True):
        if use_meta:
            x = self.meta.cut_tensor(x)[0]
        return self.core.embedding(self.adapter_a(x))

    def forward(self, x, use_meta=True):
        if use_meta:
            x = self.meta.cut_tensor(x)[0]
        return self.model(x)

    def loss(self, x, alpha_classifier=0.1, alpha_sim=0.5, priors=None,
             grad_adapters=True, grad_priors=True, grad_core=True,
             ret_all=False, return_embedding=False):
        ad_a = self.adapter_a
        ad_b = self.adapter_b
        core = self.core
        if priors is None:
            priors = list()
        for prior in priors:
            prior.requires_grad_(grad_priors)
        ad_a.requires_grad_(grad_adapters)
        ad_b.requires_grad_(grad_adapters)
        core.requires_grad_(grad_core)
        lf_mse = 0
        lf_clf = 0
        lf_sim = 0
        loss = self.mse_loss
        x, x_cl = self.meta.cut_tensor(x)
        x0 = x
        x = core.encode(ad_a(x))
        for prior, xl in zip(priors, x_cl):
            lf_clf += prior.loss(x, xl)
        if priors:
            lf_clf /= len(priors)
        lf_mse = loss(ad_b(core.decode(x)), x0)
        lf = lf_mse + alpha_sim * lf_sim + alpha_classifier * lf_clf
        if not ret_all:
            if return_embedding:
                return lf, x
            return lf
        if not return_embedding:
            return lf, (lf_mse, lf_clf, lf_sim)
        return lf, (lf_mse, lf_clf, lf_sim), x
