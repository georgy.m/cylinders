#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Generic placeholder BioNet."""
import torch
import torch.nn as nn
import numpy as np
from .core import Core


class GenericCore(Core):
    def __init__(self, n:int=1000, m:int=5, h:int=3):
        super().__init__(n, m)
        ns = np.arange(m, n, (n - m) // h)
        ns[-1] = n
        layers = list()
        for i in range(1, len(ns)):
            layers += [nn.Linear(ns[i - 1], ns[i]), nn.SELU()]
        self.decoder = nn.Sequential(*layers)
        ns = ns[::-1]
        layers = list()
        for i in range(1, len(ns)):
            layers += [nn.Linear(ns[i - 1], ns[i]), nn.SELU()]
        self.encoder = nn.Sequential(*layers)
        self.apply(self.seq_init_weights)
    
    def encode(self, x):
        return self.encoder(x)
    
    def decode(self, x):
        return self.decoder(x)

    def forward(self, x):
        return self.decoder(self.encoder(x))

    @staticmethod
    def seq_init_weights(m):
        if isinstance(m, nn.Linear):
            n = m.in_features
            torch.nn.init.normal_(m.weight, std= 1 / n ** 0.5)
            m.bias.data.zero_()
    