#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Placeholder BioNet."""
import torch
import torch.nn as nn
from .core import Core


class TinyCore(Core):

    def __init__(self):
        super().__init__(Encoder.input_size, Encoder.output_size)
        
        self.encoder = Encoder()
        self.decoder = Decoder()
        self.apply(self.seq_init_weights)
    
    def encode(self, x):
        return self.encoder(x)
    
    def decode(self, x):
        return self.decoder(x)

    def forward(self, x):
        return self.decoder(self.encoder(x))
    
    def get_input_dim(self):
        return self.encoder.get_input_size()

    def get_embedding_dim(self):
        return self.encoder.get_embedding_size()

    @staticmethod
    def seq_init_weights(m):
        if isinstance(m, nn.Linear):
            n = m.in_features
            torch.nn.init.normal_(m.weight, std= 1 / n ** 0.5)
            m.bias.data.zero_()


class Encoder(nn.Module):
    input_size = 1024
    output_size = 2
    def __init__(self):
        super().__init__()
        layers = [
            nn.Linear(Encoder.input_size, 512),
            nn.SELU(),
            nn.Linear(512, 256),
            nn.SELU(),
            nn.Linear(256, 32),
            nn.SELU(),
            nn.Linear(32, 8),
            nn.SELU(),
            nn.Linear(8, 3),
            nn.SELU(),
            nn.Linear(3, Encoder.output_size)
            ]
        self.net = nn.Sequential(*layers)
        self.apply(self.seq_init_weights)

    def forward(self, x):
        return self.net(x)
    
    @staticmethod
    def seq_init_weights(m):
        if isinstance(m, nn.Linear):
            n = m.in_features
            torch.nn.init.normal_(m.weight, std= 1 / n ** 0.5)
            m.bias.data.zero_()

class Decoder(nn.Module):
    output_size = 1024
    input_size = 2
    def __init__(self):
        super().__init__()
        layers = [
            nn.Linear(512, Decoder.output_size),
            nn.SELU(),
            nn.Linear(256, 512),
            nn.SELU(),
            nn.Linear(32, 256),
            nn.SELU(),
            nn.Linear(8, 32),
            nn.SELU(),
            nn.Linear(3, 8),
            nn.SELU(),
            nn.Linear(Decoder.input_size, 3),
            nn.SELU()
            ][::-1]
        self.net = nn.Sequential(*layers)
        self.apply(self.seq_init_weights)

    def forward(self, x):
        return self.net(x)
    
    @staticmethod
    def seq_init_weights(m):
        if isinstance(m, nn.Linear):
            n = m.in_features
            torch.nn.init.normal_(m.weight, std= 1 / n ** 0.5)
            m.bias.data.zero_()

