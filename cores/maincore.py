#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Placeholder BioNet."""
import torch
import torch.nn as nn
from .core import Core


class MainCore(Core):

    def __init__(self):
        super().__init__(Encoder.input_size, Encoder.output_size)
        
        self.encoder = Encoder()
        self.decoder = Decoder()
        self.apply(self.seq_init_weights)
    
    def encode(self, x):
        return self.encoder(x)
    
    def decode(self, x):
        return self.decoder(x)

    def forward(self, x):
        return self.decoder(self.encoder(x))
    
    def get_input_dim(self):
        return self.encoder.get_input_size()

    def get_embedding_dim(self):
        return self.encoder.get_embedding_size()

    @staticmethod
    def seq_init_weights(m):
        if isinstance(m, nn.Linear):
            n = m.in_features
            torch.nn.init.normal_(m.weight, std= 1 / n ** 0.5)
            m.bias.data.zero_()


class Encoder(nn.Module):
    input_size = 1024
    output_size = 2
    def __init__(self):
        super().__init__()
        layers = [
            nn.Linear(Encoder.input_size, Encoder.input_size),
            nn.SELU(),
            nn.Linear(Encoder.input_size, 512),
            nn.SELU(),
            nn.Linear(512, 256),
            nn.SELU(),
            ResBlock(256, 256, 128),
            ResBlock(128, 128, 64),
            ResBlock(64, 64, 64),
            ResBlock(64, 32, 16),
            nn.Linear(16, 3),
            nn.SELU(),
            nn.Linear(3, 3),
            nn.SELU(),
            nn.Linear(3, Encoder.output_size)
            ]
        self.net = nn.Sequential(*layers)
        self.apply(self.seq_init_weights)

    def forward(self, x):
        return self.net(x)
    
    @staticmethod
    def seq_init_weights(m):
        if isinstance(m, nn.Linear):
            n = m.in_features
            torch.nn.init.normal_(m.weight, std= 1 / n ** 0.5)
            m.bias.data.zero_()

class Decoder(nn.Module):
    output_size = 1024
    input_size = 2
    def __init__(self):
        super().__init__()
        layers = [
            nn.Linear(Decoder.output_size, Decoder.output_size),
            nn.SELU(),
            nn.Linear(512, Decoder.output_size),
            nn.SELU(),
            nn.Linear(256, 512),
            nn.SELU(),
            ResBlock(128, 256, 256),
            ResBlock(64, 128, 128),
            ResBlock(64, 64, 64),
            ResBlock(16, 32, 64),
            nn.Linear(3, 16),
            nn.SELU(),
            nn.Linear(Decoder.input_size, 3),
            nn.SELU(),
            ][::-1]
        self.net = nn.Sequential(*layers)
        self.apply(self.seq_init_weights)

    def forward(self, x):
        return self.net(x)
    
    @staticmethod
    def seq_init_weights(m):
        if isinstance(m, nn.Linear):
            n = m.in_features
            torch.nn.init.normal_(m.weight, std= 1 / n ** 0.5)
            m.bias.data.zero_()


class ResBlock(nn.Module):
    def __init__(self, n: int, m: int, out: int, r=3):
        super().__init__()
        layers = [nn.Linear(n, m), nn.SELU()]
        for i in range(r - 2):
            layers.append(nn.Linear(m, m))
            layers.append(nn.SELU())
        layers.append(nn.Linear(m, out))
        layers.append(nn.SELU())
        self.seq = nn.Sequential(*layers)
        self.side = nn.Sequential(nn.Linear(n, out), nn.SELU())
        self.apply(self.seq_init_weights)
    
    def forward(self, x):
        return self.side(x) + self.seq(x)

    @staticmethod
    def seq_init_weights(m):
        if isinstance(m, nn.Linear):
            n = m.in_features
            torch.nn.init.normal_(m.weight, std= 1 / n ** 0.5)
            m.bias.data.zero_()
