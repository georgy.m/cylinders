# -*- coding: utf-8 -*-
from torch import Tensor
from torch.nn import Module
from abc import ABCMeta, abstractmethod


class Core(Module, metaclass=ABCMeta):
    def __init__(self, input_size: int, embedding_size: int):
        super().__init__()
        self.input_size = input_size
        self.embedding_size = embedding_size

    @abstractmethod
    def encode(self, x: Tensor) -> Tensor:
        pass

    @abstractmethod
    def decode(self, x: Tensor) -> Tensor:
        pass
        
    
