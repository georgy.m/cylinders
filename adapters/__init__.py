# -*- coding: utf-8 -*-
from . import generic
from . import fixed
from .classifier import GenericClassifier as Classifier
from .rotator import Rotator


__adapters = {'': fixed.get_adapter,
              'generic': generic.get_adapter,
              'fixed': fixed.get_adapter}

def get_adapter(n: int, core, meta):
    n -= meta.get_count_start()
    info = iter(map(lambda x: x.strip(), meta.adapter.split(',')))
    name = next(info)
    params = dict()
    for param in info:
        a, b = map(lambda x: x.strp(), param.split(':'))
        try:
            b = float(b)
            if b == int(b):
                b = int(b)
        except ValueError:
            pass
        params[a] = b
    a, b = __adapters[name](n, core.input_size, **params)
    priors = [Classifier(j-i, core.embedding_size, name=col)
              for col, i, j in meta.cols]
    rotator = Rotator(core.embedding_size)
    return a, b, rotator, priors
    