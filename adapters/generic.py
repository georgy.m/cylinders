#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""The dynamic adapter adapter."""
import torch
import torch.nn as nn
import numpy as np


class GenericAdapter(nn.Module):
    def __init__(self, n: int, m: int, h :int=1):
        super().__init__()
        layers = list()
        h += 1
        if n > m:
            ns = np.arange(m, n, (n - m) // (h + 1))
            ns[-1] = n
            ns = ns[::-1]
        else:
            ns = np.arange(n, m, (m - n) // (h + 1))
            ns[-1] = m
        for i in range(1, len(ns)):
            a = ns[i -1]
            b = ns[i]
            layers += [nn.Linear(a, b), nn.SELU()]
        self.linear_stack = nn.Sequential(*layers)
        self.apply(self.seq_init_weights)
    
    def forward(self, x):
        return self.linear_stack(x)

    @staticmethod
    def seq_init_weights(m):
        if isinstance(m, nn.Linear):
            n = m.in_features
            torch.nn.init.normal_(m.weight, std= 1 / n ** 0.5)
            m.bias.data.zero_()

def get_adapter(n: int, m: int, h: int=1, **kwargs):
    return GenericAdapter(n, m, h), GenericAdapter(m, n, h)

