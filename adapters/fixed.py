#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""The adaper of fixed dimensions"""
import torch
import torch.nn as nn


class FixedAdapter(nn.Module):
    def __init__(self, n: int, m: int, inverse=False):
        super().__init__()
        if inverse:
            layers = [
                nn.SELU(),
                nn.Linear(m, 2048),
                nn.SELU(),
                nn.Linear(2048, n)
                ]
        else:
            layers = [
                nn.Linear(n, 2048),
                nn.SELU(),
                nn.Linear(2048, m),
                nn.SELU()
                ]
        self.linear_stack = nn.Sequential(*layers)
        self.apply(self.seq_init_weights)
    
    def forward(self, x):
        return self.linear_stack(x)

    @staticmethod
    def seq_init_weights(m):
        if isinstance(m, nn.Linear):
            n = m.in_features
            torch.nn.init.normal_(m.weight, std= 1 / n ** 0.5)
            m.bias.data.zero_()

def get_adapter(n: int, m: int, h: int=1, **kwargs):
    return FixedAdapter(n, m), FixedAdapter(n, m, True)

