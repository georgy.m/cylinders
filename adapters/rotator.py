#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Orthogonal layer. The name is perhaps misleading, but orthogonal transform
is a rotoreflection which can be a reflection as well."""
import torch.nn as nn


class Rotator(nn.Module):
    def __init__(self, n: int):
        super().__init__()

    def forward(self, x):
        return x

