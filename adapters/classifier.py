# -*- coding: utf-8 -*-
import torch.nn as nn


class GenericClassifier(nn.Module):
    def __init__(self, n: int, m: int, name: str):
        super().__init__()
        layers = [nn.Linear(m, 2 * m), nn.SELU(),
                  nn.Linear(2 * m,  2 * m), nn.SELU(),
                  nn.Linear(2 * m, n)]
        self.net = nn.Sequential(*layers)
        self._loss = nn.CrossEntropyLoss()
        self.name = name
    
    def forward(self, x):
        return self.net(x)
    
    def loss(self, x, y):
        return self._loss(self.net(x), y.max(dim=1).indices)
